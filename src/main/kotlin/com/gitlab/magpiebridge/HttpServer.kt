package com.gitlab.magpiebridge;

import com.sun.net.httpserver.HttpServer
import magpiebridge.core.*
import java.io.IOException
import java.net.InetSocketAddress
import java.net.URI
import java.net.URISyntaxException
import java.util.logging.Logger

private val LOG = Logger.getLogger("httpserver")

class HttpServer {
    val socket = InetSocketAddress("localhost", 0)
    val server = HttpServer.create(socket, 0)
    val context = server.createContext("/config")

    fun createAndStartLocalHttpServer(magpieServer: MagpieServer): String {
        try {
            context.handler = HttpHandler(magpieServer, server.address.toString())
            server.start()
            val url = URI("http", server.address.toString() + "/config", null)
            LOG.info("running on " + url.toURL().toString())
            return url.toURL().toString()
        } catch (e: IOException) {
            LOG.warning(e.message)
            MagpieServer.ExceptionLogger.log(e)
            e.printStackTrace()
        } catch (e: URISyntaxException) {
            LOG.warning(e.message)
            MagpieServer.ExceptionLogger.log(e)
            e.printStackTrace()
        }
        return ""
    }
}
