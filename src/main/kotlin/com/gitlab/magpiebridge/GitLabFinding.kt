package com.gitlab.magpiebridge

class GitLabFinding(
    var fileName: String,
    var msg: String,
    var startLine: Int,
    var endLine: Int,
    var repair: String,
    var severity: String,
    var cwes: List<String>,
){
    override fun toString(): String {
        val builder = StringBuilder()
        builder.append("filename:")
        builder.append(fileName)
        builder.append('\n')
        builder.append("msg:")
        builder.append(msg)
        builder.append('\n')
        builder.append("startLine:")
        builder.append(startLine)
        builder.append('\n')
        builder.append("endLine:")
        builder.append(endLine)
        builder.append('\n')
        return builder.toString()
    }
}