package com.gitlab.magpiebridge

import com.google.gson.Gson
import com.google.gson.JsonIOException
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import com.moandjiezana.toml.Toml
import org.apache.commons.lang3.tuple.MutablePair
import java.io.*
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.attribute.PosixFilePermissions
import java.util.logging.Logger
import kotlin.io.path.copyTo
import kotlin.io.path.deleteIfExists
import kotlin.io.path.pathString
import kotlin.io.path.writeText


class Utils {
    companion object {
        private val LOG = Logger.getLogger(Utils::class.simpleName)

        fun runAnalysis(project: String, dockerurl: String, analyzer: String, targetdir: String) {
            LOG.info("run analysis for $project, $dockerurl, $analyzer, $targetdir")
            val reportFile = "gl-sast-report.json"
            val targetReport = "sast-$analyzer.json"

            var code = """
#/bin/bash
$(which docker)  run -v "${'$'}1:/app" -m 8000M -t "${'$'}2" /bin/sh -c "cd /app && /analyzer run && chmod 777 $reportFile"
exit 0
            """.trimIndent()

            val tmpfile = kotlin.io.path.createTempFile(Paths.get("/tmp"), null, ".sh", PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwx------")))
            tmpfile.writeText(code)

            var dockerCmd = "${tmpfile.pathString} $project $dockerurl"

            try {
                val process = Runtime.getRuntime().exec(dockerCmd)
                val reader = BufferedReader(
                    InputStreamReader(process.inputStream)
                )
                var line: String?
                while (reader.readLine().also { line = it } != null) {
                    println(line)
                }
                reader.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            if (!File(targetdir).exists()) {
                File(targetdir).mkdir()
            }
            val source = Paths.get(project, reportFile)
            val target = Paths.get(targetdir, targetReport)
            source.copyTo(target, true)
            tmpfile.deleteIfExists()
        }

        fun parseTomlConfig(configPath: String): Configuration {
            var map = HashMap<String, ConfigurationEntry>()
            try {
                val toml = Toml().read(configPath)
                val tables = toml.getTables("analyzers")
                var tmpdir = toml.getString("tmpdir")

                for (tab in tables) {
                    val name = tab.getString("name")
                    val url = tab.getString("url")
                    for (xt in tab.getString("extensions").split(',')) {
                        LOG.info("Adding ${name} for ${xt} with url ${url}")
                        map.put(xt, ConfigurationEntry(name, url))
                    }
                }
                return Configuration(tmpdir, map)
            } catch (e: Exception) {
                LOG.warning(e.message)
            }
            return Configuration("", HashMap())
        }

        fun extractResults(projectPath: String, filter: Set<String> = HashSet()): Set<GitLabFinding> {
            LOG.info("parsing json reports in $projectPath")
            val result: MutableSet<GitLabFinding> = HashSet()

            if (!File(projectPath).exists()) {
                LOG.info("$projectPath not present yet")
                return result
            }

            File(projectPath).walk()
                .filter { !it.isDirectory }
                .filter { it.toString().endsWith(".json") }
                .forEach {
                println(it.toString())

                try {
                    val jsonString = File(it.toString()).readText(Charsets.UTF_8)
                    val obj = Gson().fromJson(jsonString, JsonObject::class.java)
                    for (vuln in obj.getAsJsonArray("vulnerabilities")) {
                        val jsonObj = vuln.asJsonObject

                        val message = jsonObj.get("message").asString
                        val location = jsonObj.getAsJsonObject("location").asJsonObject
                        val startLine: Int = location.get("start_line").asInt
                        val endLine: Int = location.get("start_line").asInt
                        val file = location.get("file").asString

                        var cwes = ArrayList<String>()
                        val identifiers = jsonObj.getAsJsonArray("identifiers")
                        for(identifier in identifiers) {
                            if (identifier.asJsonObject.get("type").asString == "CWE") {
                                cwes.add(identifier.asJsonObject.get("value").asString)
                            }
                        }
                        val severity = jsonObj.get("severity").asString

                        if (filter.contains(File(file).extension.removePrefix("."))) {
                            result.add(GitLabFinding(file, message, startLine, endLine, "", severity, cwes))
                        }

                    }
                } catch (e: JsonIOException) {
                    throw RuntimeException(e)
                } catch (e: JsonSyntaxException) {
                    throw RuntimeException(e)
                } catch (e: FileNotFoundException) {
                    throw RuntimeException(e)
                } finally {
                    it.delete()
                }
            }
            return result
        }

        fun columnsFromLine(infile: URL, startLine: Int, endLine: Int): MutablePair<Int,Int> {
            val columnPair: MutablePair<Int, Int> = MutablePair(1, 1)
            val file = File(infile.file)
            try {
                var lines = BufferedReader(FileReader(file)).useLines { it.toList() }
                var i = 1
                for (line in lines) {
                    if (i != startLine && i != endLine) {
                        i++
                        continue
                    }

                    var nline = line.split("//").get(0)
                    if (i == startLine) {
                        var cidx = 0
                        while (cidx < nline.toCharArray().size &&
                            (nline.toCharArray()[cidx] == ' ' || nline.toCharArray()[cidx] == '\t')
                        ) {
                            cidx += 1
                        }

                        columnPair.left = Math.max(cidx, 1)
                    }
                    if (i == endLine) {
                        var cidx = Math.max(nline.toCharArray().size - 1, 0)
                        while (cidx > 0 && nline.toCharArray()[cidx] == ' ') {
                            cidx -= 1
                        }
                        columnPair.right = cidx + 1
                    }
                    i++
                }
            } catch (e: IOException) {
                e.stackTrace
            }
            return columnPair
        }
    }
}