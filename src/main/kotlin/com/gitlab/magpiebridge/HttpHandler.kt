package com.gitlab.magpiebridge

import com.sun.net.httpserver.HttpExchange
import magpiebridge.core.MagpieServer
import magpiebridge.core.analysis.configuration.MagpieHttpHandler
import java.util.logging.Logger

private val LOG = Logger.getLogger("handler")

class HttpHandler(magpieServer: MagpieServer, address: String) : MagpieHttpHandler(magpieServer, address) {
    var findings: MutableSet<GitLabFinding> = HashSet()
        set(value) {
            field = value
        }

    override fun handle(exchange: HttpExchange?) {
        LOG.info("handle")
        val uri = exchange!!.requestURI
        val outputStream = exchange.responseBody
        val htmlPage = HtmlTemplate.generate(findings)

        exchange!!.sendResponseHeaders(200, htmlPage.length.toLong())
        outputStream.write(htmlPage.toByteArray())
        outputStream.flush()
        outputStream.close()
    }
}
