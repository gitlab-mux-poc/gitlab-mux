package com.gitlab.magpiebridge

class Configuration(var tmpdir: String, var entries: HashMap<String, ConfigurationEntry>)

class ConfigurationEntry(
    val name: String,
    val url: String,
)