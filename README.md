# gitlab-mux

GitLab Mux is an all-in-one LSP server that digests GitLab security reports 
and presents the results in any IDE based on http://github.com/MagpieBridge/MagpieBridge

GitLab Mux can be installed by running the commands below:

``` bash
gradle clean build
java -jar build/libs/magpiebridge-gitlab-1.0-SNAPSHOT.jar -c config.toml -i <project_path>
```

The `<project_path>` points to the directory where the project is located. The `config.toml` 
file includes the (dockerized) analyzers that are executed where the `url` includes the URL of the 
Docker image from the GitLab registry and the `extensions` field includes a comma-separated list of 
extensions that trigger the analyzer to be executed.

```toml
tmpdir = "/tmp/out"

[[analyzers]]
    name = "gosec"
    url = "registry.gitlab.com/gitlab-org/security-products/analyzers/gosec:latest"
    extensions = "go"

[[analyzers]]
    name = "bandit"
    url = "registry.gitlab.com/gitlab-org/security-products/analyzers/bandit:latest"
    extensions = "py"
```

